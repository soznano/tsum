//
//  GeneralProvider.swift
//  tsum
//
//  Created by George on 20/06/2017.
//  Copyright © 2017 George. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

var networkActivityIndicatorCounter = 0

extension ObservableType where E == Response {
    
    /// Maps data received from the signal into a JSON object. If the conversion fails, the signal errors.
    public func showNetworkActivityIndicator() -> Observable<Response> {
        networkActivityIndicatorCounter += 1
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.subscribe { event in
            networkActivityIndicatorCounter -= 1
            if networkActivityIndicatorCounter <= 0 {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        
        return self.asObservable()
    }
}
