//
//  RequestService.swift
//  tsum
//
//  Created by George on 20/06/2017.
//  Copyright © 2017 George. All rights reserved.
//

import UIKit
import Foundation
import Moya
import Alamofire

enum RequestService {
    case translateSingle(word: String, sourceLanguage: String, translateLanguage: String)
}

extension RequestService: TargetType {
    
    var baseURL: URL { return URL(string: "http://translate.google.com")! }
    var path: String {
        switch self {
        case .translateSingle:
            return "/translate_a/single"
        }
    }
    var method: Moya.Method {
        switch self {
        case .translateSingle:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .translateSingle(let word, let sLanguage, let tLanguage):
            return ["client": "gtx", "sl": sLanguage, "tl": tLanguage,
                    "dt": ["at", "bd", "ex", "ld", "md", "qca", "rw", "rm", "ss", "t"],
                    "q": word]
        }
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        switch self {
        case .translateSingle:
            return URLArrayEncoding.default // Send parameters in URL
        }
    }
    var sampleData: Data {
        return Data()
    }
    var task: Task {
        switch self {
        case .translateSingle:
            return .request
        }
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
