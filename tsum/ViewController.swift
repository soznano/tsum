//
//  ViewController.swift
//  tsum
//
//  Created by George on 20/06/2017.
//  Copyright © 2017 George. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let provider = RxMoyaProvider<RequestService>()
        provider.request(.translateSingle(word: "Hello", sourceLanguage: "en", translateLanguage: "ru"))
            .showNetworkActivityIndicator()
            .mapJSON()
            .subscribe { event in
                switch event {
                case .next(let response):
                    print(response)
                default:
                    break
                }
        }
    }

}

